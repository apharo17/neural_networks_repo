#------------------------------------
#Abner Perez Haro
#Introduction to Neural Networks and Deep Learning
#Homework 1 part A: single perceptron
#------------------------------------

import random
import matplotlib.pyplot as plt
import numpy as np
from homework1_util import *


#----------Input parameters----------
data_file = 'dataset_c.txt'



input_data = []
labels = []
read_data(data_file, input_data, labels)
m = len(labels)

#plot data without normalization
input_data_x,input_data_y = zip(*input_data)
for i in range(m):
    if labels[i] ==1:
        plt.plot(input_data_x[i], input_data_y[i], 'ro')
    else:
        plt.plot(input_data_x[i], input_data_y[i], 'gx')
plt.axvline(x=0)
plt.axhline(y=0)
plt.autoscale(False)
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.show()


#normalize data
input_data = norm_data(input_data)
#create matrices and vectors for training
np_x = np.array(input_data)
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)
#train perceptron
np_w = run_training(m, np_x, np_labels, 0.75, 0.001)


#generate the boundary line
axis_x_max = 2.0
x_step = 0.1
x = np.arange(-axis_x_max, axis_x_max, x_step)
l = -np_w[1]/np_w[2]*x -np_w[0]/np_w[2]


#Plot classified data
input_data_x,input_data_y = zip(*input_data)
for i in range(m):
    if labels[i] ==1:
        plt.plot(input_data_x[i], input_data_y[i], 'ro')
    else:
        plt.plot(input_data_x[i], input_data_y[i], 'gx')

plt.plot(x,l,'-b')
plt.axvline(x=0)
plt.axhline(y=0)
plt.autoscale(False)
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.show()


