import random
import numpy as np

def read_data(file_name, input_data, labels):
    """creates a list of tuples input_data containing the training
    data points, and the list of corresponding labels reading file_name"""

    f = open(file_name,'r')
    for line in f:
        arr = line[:-1].split()
        input_data.append((float(arr[0]),float(arr[1])))
        labels.append(int(arr[2]))
    f.close()

def norm_data(data):
    """returns a new list with the normalized data"""
    min_x = min([x for x,_ in data])
    max_x = max([x for x,_ in data])
    min_y = min([y for _,y in data])
    max_y = max([y for _,y in data])
    n_data = [((x-min_x)/(max_x-min_x), (y-min_y)/(max_y-min_y)) for x,y in data]
    return n_data


def run_training(m, np_x, np_d, l_rate, max_error):

    """trains a single perceptron from a training matrix np_x (with w samples) and
    a label vector np_d"""

    print('-----Running training-----')
    n = 0
    error = 1
    np_w = np.array([0] * np_x.shape[1])
    np_y = np.array([0]*m) # dummy initialization

    while error > max_error:
        for i in range(m):
            np_y[i] = 1.0
            if np_w.dot(np_x[i]) < 0:
                np_y[i] = -1.0
            #update weights
            np_w = np_w + l_rate*(np_d[i] - np_y[i])*np_x[i]

        #calculate error
        error = (np_d - np_y).dot(np_d - np_y) / m
        str_output = 'Error at iteration {0}: {1}'.format(n+1,error)
        print(str_output)

        n += 1

    return np_w