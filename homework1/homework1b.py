#------------------------------------
#Abner Perez Haro
#Introduction to Neural Networks and Deep Learning
#Homework 1 part B: training two perceptrons
#------------------------------------

import random
import math
import matplotlib.pyplot as plt
import numpy as np
from homework1_util import *
from mpl_toolkits.mplot3d import Axes3D

#----------Input parameters----------
data_file = 'dataset_c.txt'



input_data = []
labels = []
read_data(data_file, input_data, labels)
m = len(labels)

#normalize data
input_data = norm_data(input_data)

#create matrices and vectors for first training
np_x = np.array(input_data)
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)
#train perceptron
print('First training:')
np_w = run_training(m, np_x, np_labels, 0.75, 0.001)


#get output from the first perceptron
np_v = np_x.dot(np_w)


#apply a relu function to np_v
np_y1 = np.array([0]*m)
i=0
for a in np_v:
    if a > 0:
        np_y1[i] = a
    i+=1

#apply a tanh function to np_v
np_y2 = np.tanh(np_v)

#apply a sigmoid function to np_v
np_y3 = 1 / (1 + np.exp(-np_v))


#create matrices and vectors for second training
np_x = np.array([[np_y1[i],np_y2[i],np_y3[i]] for i in range(m)])
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)
#train perceptron
print('First training:')
np_w = run_training(m, np_x, np_labels, 0.75, 0.001)

#create the boundary plane
step = 0.1
x = np.arange(-1, 1, step)
y = np.arange(-1, 1, step)
xx, yy = np.meshgrid(x, y)
z = -np_w[2]/np_w[3]*yy -np_w[1]/np_w[3]*xx -np_w[0]/np_w[3]

#plot data
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(m):
    if labels[i] ==1:
        ax.scatter(np_y1[i], np_y2[i], np_y3[i], c='r', marker='o')
    else:
        ax.scatter(np_y1[i], np_y2[i], np_y3[i], c='g', marker='x')

ax.contour3D(xx, yy, z, 50, cmap='binary')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()


