import random
import math
import matplotlib.pyplot as plt
import numpy as np


MIN_DELTA_X = 0.005

def rand_cluster(n,c,r):
    """returns n random points in disk of radius r centered at c"""
    x,y = c
    points = []
    for i in range(n):
        theta = 2*math.pi*random.random()
        s = r*random.random()
        points.append((s*math.cos(theta)+x, s*math.sin(theta)+y))
    return points


def create_clusters(slope,b,r,m_clusterA,m_clusterB):

    cluster_a = rand_cluster(m_clusterA, (0.0, r), 0.99 * r)
    cluster_b = rand_cluster(m_clusterB, (0.0, -r), 0.99 * r)

    theta = math.atan(slope)
    cluster_a = rotate_points(cluster_a, theta)
    cluster_b = rotate_points(cluster_b, theta)

    cluster_a = move_points_vertical(cluster_a, b)
    cluster_b = move_points_vertical(cluster_b, b)

    return [cluster_a,cluster_b]


def gen_slope():
    delta_x = random.random()
    delta_y = random.random()
    if delta_y < MIN_DELTA_X:
        delta_y = MIN_DELTA_X
    slope = delta_y / delta_x
    if random.randint(0, 1) == 0:
        slope *= -1
    return slope


def rotate_points(points, theta):
    """returns a new list of rotated points by theta in radians"""
    points_ = []
    for (x,y) in points:
        x_ = x  *math.cos(theta) - y * math.sin(theta)
        y_ = x * math.sin(theta) + y * math.cos(theta)
        points_.append((x_,y_))
    return points_


def move_points_vertical(points,delta):
    """return a new list of displaced points by delta in the vertical axis"""
    points_ = []
    for (x, y) in points:
        y = y + delta
        points_.append((x, y))
    return points_



#----------------------------------------------------------------------------------------
m_clusterA = 100
m_clusterB = 100


m = m_clusterA + m_clusterB
slope = gen_slope()
#slope = 0.8008635828559046
b = 20*(random.random() - 0.5)
r = 2
[clusterA,clusterB] = create_clusters(slope,b,r,m_clusterA,m_clusterB)

indexes = list(range(m))
random.shuffle(indexes)
input_data = [0]*m
labels = [0]*m

print('b = ',b)
print('slope = ',slope)

for i in range(m):
    if indexes[i] < m_clusterA:
        input_data[i] = clusterA[indexes[i]]
        labels[i] = 1
    else:
        input_data[i] = clusterB[indexes[i]-m_clusterA]
        labels[i] = -1



AXIS_X_MAX = 2.0
X_STEP = 0.1
x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
y = slope*x+b
w0 = -1/slope*x + b

clusterA_x,clusterA_y = zip(*clusterA)
clusterB_x,clusterB_y = zip(*clusterB)


np_x = np.array(input_data)
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)


plt.plot(clusterA_x,clusterA_y,'ro')
plt.plot(clusterB_x,clusterB_y,'gx')
plt.plot(x,y)
plt.axvline(x=0)
plt.axhline(y=0)
plt.autoscale(False)
plt.grid()
plt.show()


#-----------------------------------------------------------------------------
input_data_x,input_data_y = zip(*input_data)

f = open('dataset.txt','w')

for i in range(len(input_data)):
    str_output = '{0} {1} {2}\n'.format(input_data_x[i], input_data_y[i], labels[i])
    f.write(str_output)

f.close()