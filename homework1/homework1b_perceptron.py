import random
import math
import matplotlib.pyplot as plt
import numpy as np

def read_data(file_name, input_data, labels):
    """creates a list of tuples input_data containing the
    training data points, and the list of corresponding labels reading file_name"""

    f = open(file_name,'r')

    for line in f:
        arr = line[:-1].split()
        x_coor = float(arr[0])
        y_coor = float(arr[1])
        input_data.append((x_coor,y_coor))
        labels.append(int(arr[2]))
    f.close()

def norm_data(data):
    """returns a new list with the normalized data"""
    min_x = min([x for x,_ in data])
    max_x = max([x for x,_ in data])
    min_y = min([y for _,y in data])
    max_y = max([y for _,y in data])
    n_data = [((x-min_x)/(max_x-min_x), (y-min_y)/(max_y-min_y)) for x,y in data]

    return n_data


def run_training(m, np_x, np_d, l_rate, max_error):
    #np_w = np.array([.5*random.random()-.25] * 3)
    #print(np_w)
    np_w = np.array([0] * np_x.shape[1])

    n = 0

    # dummy initialization
    np_y = np.array([0]*m)

    error = (np_d - np_y).dot(np_d - np_y)/m
    #while (np_d - np_y).dot(np_d - np_y)/m > max_error:
    while error > max_error:
        for i in range(m):
            np_y[i] = 1.0
            if np_w.dot(np_x[i]) < 0:
                np_y[i] = -1.0
            np_w = np_w + l_rate*(np_d[i] - np_y[i])*np_x[i]

        n += 1
        error = (np_d - np_y).dot(np_d - np_y) / m
        print(error)
        if n > 50:
            break

    print('n =',n)
    return np_w



#------------------------------------------------------------------------------------------
input_data = []
labels = []
read_data('dataset_c.txt', input_data, labels)
m = len(labels)
input_data = norm_data(input_data)

np_x = np.array(input_data)
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)
np_w = run_training(m, np_x, np_labels, 0.75, 0.001)

AXIS_X_MAX = 2.0
X_STEP = 0.1
x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
l = -np_w[1]/np_w[2]*x -np_w[0]/np_w[2]


input_data_x,input_data_y = zip(*input_data)
plt.plot(input_data_x,input_data_y,'ro')
plt.plot(x,l,'-g')
plt.axvline(x=0)
plt.axhline(y=0)
plt.autoscale(False)
plt.grid()
plt.show()




#------------------------------------------------------------------------------------------
print('Segunda ronda')
from mpl_toolkits.mplot3d import Axes3D

np_v = np_x.dot(np_w)
np_y1 = np.array([0]*m)

i=0
for a in np_v:
    if a > 0:
        np_y1[i] = a
    i+=1

np_y2 = np.tanh(np_v)
np_y3 = 1 / (1 + np.exp(-np_v))


np_x = np.array([[np_y1[i],np_y2[i],np_y3[i]] for i in range(m)])
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)
np_w = run_training(m, np_x, np_labels, 0.75, 0.001)

x = np.arange(-1, 1, X_STEP)
y = np.arange(-1, 1, X_STEP)
xx, yy = np.meshgrid(x, y)
z = -np_w[2]/np_w[3]*yy -np_w[1]/np_w[3]*xx -np_w[0]/np_w[3]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(m):
    if labels[i] ==1:
        ax.scatter(np_y1[i], np_y2[i], np_y3[i], c='r', marker='o')
    else:
        ax.scatter(np_y1[i], np_y2[i], np_y3[i], c='g', marker='x')
#ax.plot_wireframe(x,y,z, rstride=2, cstride=2)
#ax.scatter(x, y, z, c=z, cmap='viridis', linewidth=0.5)

ax.contour3D(xx, yy, z, 50, cmap='binary')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_title('Dataset C',fontsize=18)
plt.show()


