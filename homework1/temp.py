import matplotlib.pyplot as plt
import numpy as np
import random

def run_training(m, np_x, np_d, l_rate, max_error, relu=False):
    np_w = np.array([0.2*random.random()] * 2)

    n = 0

    # dummy initialization
    np_y = np.array([0]*m)

    error = (np_d - np_y).dot(np_d - np_y)/m
    #while (np_d - np_y).dot(np_d - np_y)/m > max_error:
    while error > max_error:
        for i in range(m):
            if relu:
                np_y[i] = np_w.dot(np_x[i])
                if np_y[i] < 0:
                    np_y[i] = -1
            else:
                np_y[i] = 1.0
                if np_w.dot(np_x[i]) < 0:
                    np_y[i] = -1.0
            np_w = np_w + l_rate*(np_d[i] - np_y[i])*np_x[i]

        n += 1
        error = (np_d - np_y).dot(np_d - np_y) / m
        print(error)
        if n > 50:
            break

    print('n =',n)
    return np_w



def norm_cluster(data):
    mean_x = sum(x for x,y in data) / len(data)
    mean_y = sum(y for x,y in data) / len(data)
    std_d_x = (sum((x - mean_x) ** 2 for x, y in data) / len(data)) ** (1 / 2.0)
    std_d_y = (sum((y - mean_y) ** 2 for x, y in data) / len(data)) ** (1 / 2.0)
    stand_data = []

    # get standardized values
    for x, y in data:
        stand_x = (x - mean_x) / std_d_x
        stand_y = (y - mean_y) / std_d_y

        stand_data.append((stand_x, stand_y))

    print(stand_data)
    #return stand_data

    # find min/max value from the whole data
    min_v = min(min([x, y]) for x, y in stand_data)
    max_v = max(max([x, y]) for x, y in stand_data)

    norm_data = []
    for i, (stand_x, stand_y) in enumerate(stand_data):
        # normalize it
        norm_x = (stand_x - min_v) / (max_v - min_v)
        norm_y = (stand_y - min_v) / (max_v - min_v)
        norm_data.append((norm_x, norm_y))
    
    return norm_data

#-----------------------------------------------------------------------------------





m_clusterA = 2
m_clusterB = 2
m = m_clusterA + m_clusterB
clusterA = [(0.0,1.0),(0.25,0.75)]
clusterB = [(1.0,0.0),(0.75,0.25)]

#clusterA = [(0.0,1.0)]
#clusterB = [(1.0,0.0)]

indexes = list(range(m))
random.shuffle(indexes)
input_data = [0]*m
labels = [0]*m

for i in range(m):
    if indexes[i] < m_clusterA:
        input_data[i] = clusterA[indexes[i]]
        labels[i] = 1
    else:
        input_data[i] = clusterB[indexes[i]-m_clusterA]
        labels[i] = -1

n_input_data = norm_cluster(input_data)
np_x = np.array(n_input_data)
#np_x = np.array(input_data)
np_labels = np.array(labels)
np_w = run_training(m, np_x, np_labels, 0.01, 0.001, relu=True)

AXIS_X_MAX = 2.0
X_STEP = 0.1
x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
l = -np_w[0]/np_w[1]*x



n_input_data_x,n_input_data_y = zip(*n_input_data)
plt.plot(n_input_data_x,n_input_data_y,'ro')
#input_data_x,input_data_y = zip(*input_data)
#plt.plot(input_data_x,input_data_y,'ro')
plt.plot(x,l,'-g')
plt.axvline(x=0)
plt.axhline(y=0)
plt.autoscale(False)
plt.grid()
plt.show()