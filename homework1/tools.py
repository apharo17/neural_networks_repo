import random

MIN_DELTA_X = 0.005


def gen_slope():

    delta_x = random.random()
    delta_y = random.random()
    if delta_y < MIN_DELTA_X:
        delta_y = MIN_DELTA_X
    m = delta_y / delta_x
    if random.randint(0, 1) == 0:
        m *= -1
    return m

def gen_y_intercept():
    return random.random() * 30