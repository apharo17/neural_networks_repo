
import matplotlib.pyplot as plt
import numpy as np
import tools

AXIS_X_MAX = 50.0
#AXIS_Y_MAX = 50.0
X_STEP = 1.0

#m = tools.gen_slope()
#b = tools.gen_y_intercept()
m = 1.247169865618998
b = 15.690158188858582

x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
y = m*x + b
w0 = -1/m*x + b

AXIS_Y_MAX = max([y[0],y[-1],w0[0],w0[-1]])
AXIS_Y_MIN = min([y[0],y[-1],w0[0],w0[-1]])

print(m)
print(-1/m)
print(b)

#reg0 = [(x[0],y[0]), (x[-1],y[-1]), (x[0],y[0]), (x[-1],y[-1])]


plt.plot(x[0],y[0],'ro')
plt.plot(x,y)
plt.plot(x,w0)
plt.axhline(y=0)
plt.axvline(x=0)
plt.grid()
plt.xlim(-AXIS_X_MAX, AXIS_X_MAX)
plt.ylim(AXIS_Y_MIN, AXIS_Y_MAX)
plt.autoscale(False)
plt.show()