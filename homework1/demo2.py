import random
import math
import matplotlib.pyplot as plt
import numpy as np

MIN_DELTA_X = 0.005

def rand_cluster(n,c,r):
    """returns n random points in disk of radius r centered at c"""
    x,y = c
    points = []
    for i in range(n):
        theta = 2*math.pi*random.random()
        s = r*random.random()
        points.append((s*math.cos(theta)+x, s*math.sin(theta)+y))
    return points

def gen_slope():
    delta_x = random.random()
    delta_y = random.random()
    if delta_y < MIN_DELTA_X:
        delta_y = MIN_DELTA_X
    m = delta_y / delta_x
    if random.randint(0, 1) == 0:
        m *= -1
    return m

def rotate_points(points, theta):
    """returns a new list of rotated points by theta in radians"""
    points_ = []
    for (x,y) in points:
        x_ = x  *math.cos(theta) - y * math.sin(theta)
        y_ = x * math.sin(theta) + y * math.cos(theta)
        points_.append((x_,y_))
    return points_

def move_points_vertical(points,delta):
    """return a new list of displaced points by delta in the vertical axis"""
    points_ = []
    for (x, y) in points:
        y = y + delta
        points_.append((x, y))
    return points_

r = 0.5
clusterA = rand_cluster(10,(0.0,0.5),0.9*r)
clusterB = rand_cluster(10,(0.0,-0.5),0.9*r)

clusterA_x,clusterA_y = zip(*clusterA)
clusterB_x,clusterB_y = zip(*clusterB)
plt.plot(clusterA_x,clusterA_y,'ro')
plt.plot(clusterB_x,clusterB_y,'gx')
plt.axvline(x=0)
plt.axhline(y=0)
plt.show()

#------------------------------------------------


#m = gen_slope()
m = 0.8008635828559046
#print('m =',m)
theta = math.atan(m)
clusterA = rotate_points(clusterA,theta)
clusterB = rotate_points(clusterB,theta)

b = 2.0
clusterA = move_points_vertical(clusterA,b)
clusterB = move_points_vertical(clusterB,b)


AXIS_X_MAX = 2.0
X_STEP = 0.1
x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
y = m*x+b
w0 = -1/m*x + b

clusterA_x,clusterA_y = zip(*clusterA)
plt.plot(clusterA_x,clusterA_y,'ro')
clusterB_x,clusterB_y = zip(*clusterB)
plt.plot(clusterB_x,clusterB_y,'gx')

plt.plot(x,y)
plt.plot(x,w0)
plt.axvline(x=0)
plt.axhline(y=0)
plt.xlim(-2.0, 2.0)
plt.ylim(0, 4.0)
plt.autoscale(False)
plt.grid()
plt.show()