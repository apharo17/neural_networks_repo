import random
import math
import matplotlib.pyplot as plt
import numpy as np

MIN_DELTA_X = 0.005

def rand_cluster(n,c,r):
    """returns n random points in disk of radius r centered at c"""
    x,y = c
    points = []
    for i in range(n):
        theta = 2*math.pi*random.random()
        s = r*random.random()
        points.append((s*math.cos(theta)+x, s*math.sin(theta)+y))
    return points


def create_clusters(slope,b,r,m_clusterA,m_clusterB):

    cluster_a = rand_cluster(m_clusterA, (0.0, r), 0.99 * r)
    cluster_b = rand_cluster(m_clusterB, (0.0, -r), 0.99 * r)

    theta = math.atan(slope)
    cluster_a = rotate_points(cluster_a, theta)
    cluster_b = rotate_points(cluster_b, theta)

    cluster_a = move_points_vertical(cluster_a, b)
    cluster_b = move_points_vertical(cluster_b, b)

    return [cluster_a,cluster_b]


def gen_slope():
    delta_x = random.random()
    delta_y = random.random()
    if delta_y < MIN_DELTA_X:
        delta_y = MIN_DELTA_X
    slope = delta_y / delta_x
    if random.randint(0, 1) == 0:
        slope *= -1
    return slope


def rotate_points(points, theta):
    """returns a new list of rotated points by theta in radians"""
    points_ = []
    for (x,y) in points:
        x_ = x  *math.cos(theta) - y * math.sin(theta)
        y_ = x * math.sin(theta) + y * math.cos(theta)
        points_.append((x_,y_))
    return points_


def move_points_vertical(points,delta):
    """return a new list of displaced points by delta in the vertical axis"""
    points_ = []
    for (x, y) in points:
        y = y + delta
        points_.append((x, y))
    return points_




def run_training(m, np_x, np_d, l_rate, max_error):
    np_w = np.array([0.2*random.random()] * 3)

    n = 0

    # dummy initialization
    np_y = np.array([0]*m)

    error = (np_d - np_y).dot(np_d - np_y)/m
    #while (np_d - np_y).dot(np_d - np_y)/m > max_error:
    while error > max_error:
        for i in range(m):
            np_y[i] = 1.0
            if np_w.dot(np_x[i]) < 0:
                np_y[i] = -1.0
            np_w = np_w + l_rate*(np_d[i] - np_y[i])*np_x[i]

        n += 1
        error = (np_d - np_y).dot(np_d - np_y) / m
        print(error)
        if n > 50:
            break

    print('n =',n)
    print('m=',np_x)
    return np_w

#----------------------------------------------------------------------------------------
m_clusterA = 10
m_clusterB = 10


m = m_clusterA + m_clusterB
#slope = gen_slope()
slope = 0.8008635828559046
b = 2*random.random()
r = 0.5
[clusterA,clusterB] = create_clusters(slope,b,r,m_clusterA,m_clusterB)


indexes = list(range(m))
random.shuffle(indexes)
input_data = [0]*m
labels = [0]*m

print('b = ',b)
print('slope = ',slope)

for i in range(m):
    if indexes[i] < m_clusterA:
        input_data[i] = clusterA[indexes[i]]
        labels[i] = 1
    else:
        input_data[i] = clusterB[indexes[i]-m_clusterA]
        labels[i] = -1



AXIS_X_MAX = 2.0
X_STEP = 0.1
x = np.arange(-AXIS_X_MAX, AXIS_X_MAX, X_STEP)
y = slope*x+b
w0 = -1/slope*x + b

clusterA_x,clusterA_y = zip(*clusterA)
clusterB_x,clusterB_y = zip(*clusterB)

'''
plt.plot(clusterA_x,clusterA_y,'ro')
plt.plot(clusterB_x,clusterB_y,'gx')

plt.plot(x,y)
plt.plot(x,w0)
plt.axvline(x=0)
plt.axhline(y=0)
plt.xlim(-2.0, 2.0)
plt.ylim(0, 4.0)
plt.autoscale(False)
plt.grid()
plt.show()
'''

#print('clusterA =',clusterA)
#print('clusterB =',clusterB)
np_x = np.array(input_data)
np_x = np.append(np.ones((m,1)), np_x, axis=1)
np_labels = np.array(labels)


np_w = run_training(m, np_x, np_labels, 0.75, 0.001)

l = -np_w[1]/np_w[2]*x -np_w[0]/np_w[2]

plt.plot(clusterA_x,clusterA_y,'ro')
plt.plot(clusterB_x,clusterB_y,'gx')
plt.plot(x,y)
plt.plot(x,l,'-g')
plt.axvline(x=0)
plt.axhline(y=0)
#plt.xlim(-2.0, 2.0)
#plt.ylim(0, 4.0)
plt.autoscale(False)
plt.grid()
plt.show()